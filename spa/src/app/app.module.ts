import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { UploadComponent } from './components/upload/upload.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ChooseBackgroundComponent } from './components/choose-background/choose-background.component';
import { GardensComponent } from './components/gardens/gardens.component';
import { DndModule } from 'ngx-drag-drop';
import { PlantsComponent } from './components/plants/plants.component';

@NgModule({
  declarations: [
    AppComponent,
    UploadComponent,
    ChooseBackgroundComponent,
    GardensComponent,
    PlantsComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    NoopAnimationsModule,
    DndModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
