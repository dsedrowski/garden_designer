import { Plant } from './plant.model';

export class PlantType {
    constructor(
        public id: number,
        public type: string,
        public plants: Plant[]
    ) {}
}