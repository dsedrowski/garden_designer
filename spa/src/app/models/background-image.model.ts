export class BackgroundImage {
    constructor(
        public image: string,
        public id?: number
    ) {}
}