export class CreateGarden {
    constructor(
        public name: string,
        public numberOfSquares?: number,
        public squareHeight?: number,
        public squareWidth?: number
    ) {}
}