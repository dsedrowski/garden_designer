import { PlantType } from './plant-type.model';
import { Square_Plant } from './square-plant.model';

export class Plant {
    constructor(
        public id: number,
        public plantTypeId: number,
        public name: string,
        public image: string,
        public plantType: PlantType,
        public square_Plants: Square_Plant[]
    ) {}
}