import { Square } from './square.model';
import { Plant } from './plant.model';

export class Square_Plant {
    constructor(
        public squareId: number,
        public plantId: number,
        public square: Square,
        public plant: Plant,
        public id?: number
    ) {}
}