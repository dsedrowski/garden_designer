import { BackgroundImage } from './background-image.model';
import { Square } from './square.model';

export class Garden {
    constructor(
        public name: string,
        public backgroundImage?: BackgroundImage,
        public squares?: Square[],
        public id?: number,
        public backgroundImageId?: number,
    ) {}
}