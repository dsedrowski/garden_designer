import { Square_Plant } from './square-plant.model';

export class Square {
    constructor(
        public order: number,
        public width: number,
        public height: number,
        public square_Plants: Square_Plant[],
        public id?: number,     
        public gardenId?: number
    ) {}
}