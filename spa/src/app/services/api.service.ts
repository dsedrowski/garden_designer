import { Injectable } from '@angular/core';
import { Http, Request, RequestMethod } from '@angular/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Garden } from '../models/garden.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: Http) { }

  public getGardensList(): Observable<any> {
    return this.sendRequest(RequestMethod.Get, 'garden', '');
  }

  public getPlantTypesList(): Observable<any> {
    return this.sendRequest(RequestMethod.Get, 'garden/get-types', '');
  }

  public getGarden(id: number): Observable<any> {
    return this.sendRequest(RequestMethod.Get, `garden/${id}`, '');
  }

  public createGarden(data: any): Observable<any> {
    return this.sendRequest(RequestMethod.Post, 'garden', data);
  }

  public updateGarden(data: any): Observable<any> {
    return this.sendRequest(RequestMethod.Put, 'garden', data);
  }

  public uploadPhoto(file: any): Observable<any> {
    return this.sendRequest(RequestMethod.Post, 'garden/upload-photo', file);
  }

  sendRequest(verb: RequestMethod, url: string, body?: any) : Observable<any> {
    let request = new Request({
        method: verb,
        url: environment.apiUrl + url,
        body: body
    });

    return this.http.request(request)
                      .pipe(
                          map(response => {
                            if (response.status < 200 || response.status >= 300) {      
                              alert(response.text);
                            }
                            else {
                              return response;
                            }
                          }));      
  }
}
