import { Injectable } from '@angular/core';
import { Garden } from '../models/garden.model';
import { ApiService } from './api.service';
import { BackgroundImage } from '../models/background-image.model';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { PlantType } from '../models/plant-type.model';
import { Plant } from '../models/plant.model';

@Injectable({
  providedIn: 'root'
})
export class GardenService {
  public gardensList: Garden[] = null;
  public plantTypes: PlantType[] = null;
  public garden: Garden = null;
  public bgImage: BackgroundImage = new BackgroundImage("");
  public safeBackgroundPath: SafeStyle;

  constructor(private api: ApiService, private santizier: DomSanitizer) { 
    this.refreshGardens();
    this.getPlantTypes();
  }

  create(data: Garden) {
    this.api.createGarden(data).subscribe(x => {
      this.refreshGardens();
      alert('garden successfully created');
    });
  }

  save() {
    this.api.updateGarden(this.garden).subscribe(x => {
      this.refreshGardens();
      alert('garden successfully updated');
    });
  }

  selectGarden(id: number) {
    let garden = this.gardensList.filter((g: Garden) => {
      return g.id == id;
    });

    if (garden && garden.length > 0) {
      this.garden = garden[0];
      this.bgImage = this.garden.backgroundImage;
      this.safeBackgroundPath = this.makeSafeBgImagePath();
    }
  }

  changeBackground(image: string) {
    this.bgImage.image = image;
    this.safeBackgroundPath = this.makeSafeBgImagePath();

    if (this.garden)
      this.garden.backgroundImage = this.bgImage;
  }

  getGardens(): Garden[] {
    return this.gardensList;
  }

  getPlantById(id: number): Plant {
    let plant: Plant = null;

    this.plantTypes.forEach(type => {
      let filtered = type.plants.filter(x => x.id === id);

      if (filtered.length > 0) plant = filtered[0];
    });

    return plant;
  }

  refreshGardens() {
    this.api.getGardensList().subscribe(data => this.gardensList = JSON.parse(data._body));
  }

  private getPlantTypes() {
    this.api.getPlantTypesList().subscribe(data => this.plantTypes = JSON.parse(data._body));
  }

  private makeSafeBgImagePath(): SafeStyle {
    return this.santizier.bypassSecurityTrustStyle(`url("${environment.imagesUrl}${this.bgImage.image}")`);
  }
}
