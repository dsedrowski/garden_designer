import { Component, HostBinding } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Garden } from './models/garden.model';
import { environment } from 'src/environments/environment';
import { GardenService } from './services/garden.service';
import { DndDropEvent } from 'ngx-drag-drop';
import { Square } from './models/square.model';
import { Square_Plant } from './models/square-plant.model';
import { Plant } from './models/plant.model';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'spa';
  public image: any;

  constructor(public service: GardenService) {
  }

  checkSquares() {
    return this.service.garden && this.service.garden.squares;
  }

  isPlantInSquare(plant: Plant, square: Square): boolean {
    let filtered = square.square_Plants.filter(x => x.plantId === plant.id);

    return filtered.length > 0;
  }
  
  onDrop(event:DndDropEvent, square: Square) {
    if (this.isPlantInSquare(event.data, square)) {
      alert('The plant is already in this square.');
      return;
    }
  
    let plant: Plant = event.data;    
    square.square_Plants.push(new Square_Plant(square.id, event.data.id, null, null));
    plant.square_Plants = square.square_Plants;
  }

  getCountInSquare(plant: Plant): number {
    let i = 0;

    if (!this.service.garden) return i;

    this.service.garden.squares.forEach(s => {
      let sp = s.square_Plants.filter(x => x.plantId === plant.id);

      if (sp.length > 0) i++;
    })

    return i;
  }

  getPlantName(id: number): string {
    return this.service.getPlantById(id)?.name ?? '';
  }
}
