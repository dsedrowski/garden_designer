import { Component, OnInit } from '@angular/core';
import { GardenService } from 'src/app/services/garden.service';
import { Plant } from 'src/app/models/plant.model';

@Component({
  selector: 'plants',
  templateUrl: './plants.component.html',
  styleUrls: ['./plants.component.scss']
})
export class PlantsComponent implements OnInit {

  constructor(public service: GardenService) { }

  ngOnInit(): void {
  }
}
