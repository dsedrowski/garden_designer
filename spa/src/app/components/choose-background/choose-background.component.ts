import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { GardenService } from 'src/app/services/garden.service';

@Component({
  selector: 'choose-background',
  templateUrl: './choose-background.component.html',
  styleUrls: ['./choose-background.component.scss']
})
export class ChooseBackgroundComponent implements OnInit {
  constructor(private service: GardenService) { }

  ngOnInit() {
  }

  onUploadFinished(response) {
    this.service.changeBackground(response.fileName);
  }
}
