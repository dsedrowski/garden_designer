import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Garden } from 'src/app/models/garden.model';
import { CreateGarden } from 'src/app/models/create-garden.model';
import { Square } from 'src/app/models/square.model';
import { BackgroundImage } from 'src/app/models/background-image.model';
import { GardenService } from 'src/app/services/garden.service';

@Component({
  selector: 'gardens',
  templateUrl: './gardens.component.html',
  styleUrls: ['./gardens.component.scss']
})
export class GardensComponent implements OnInit {
  public formDisabled: boolean = true;
  public model: CreateGarden = new CreateGarden("");

  constructor(public service: GardenService) {}

  ngOnInit(): void {
  }

  saveGarden() {
    this.service.save();
  }

  onSubmit() {
    let squares = new Array<Square>();
    if (this.model.numberOfSquares) {
      for (let i = 0; i < this.model.numberOfSquares; i++) {
        squares.push(new Square(i, this.model.squareWidth, this.model.squareHeight, []));
      }
    }

    let garden = new Garden(this.model.name, this.service.bgImage, squares);
    this.service.create(garden);
    this.formDisabled = true;
  }

  onUnlockFormButtonClick() {
    this.formDisabled = false;
  }

  onGardenSelected(event) {
    if (event.target.value)
      this.service.selectGarden(event.target.value);
    else
      this.service.garden = null;
  }
}
