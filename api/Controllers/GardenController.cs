using Microsoft.AspNetCore.Mvc;
using api.Repositories;
using System.Threading.Tasks;
using api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using System.Net.Http.Headers;

namespace api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class GardenController : Controller
    {
        private readonly IGardenRepository _repo;

        public GardenController(IGardenRepository repo)
        {
            this._repo = repo;
        }

        [HttpGet]
        public async Task<IActionResult> Get() {
            try {
                ICollection<Garden> data = await this._repo.GetGardensListAsync();
                return Ok(data);
            } catch (Exception ex) {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get([FromRoute]int id) 
        {
            try {
                Garden entity = await this._repo.GetGardenAsync(id);

                if (entity == null)
                    return NotFound("The garden with given id does not exists.");

                return Ok(entity);
            } catch (Exception ex) {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Garden data) {
            try {
                this._repo.CreateGarden(data);
                int result = await this._repo.SaveChangesAsync();

                if (result == 0)
                    return BadRequest("An unexpected error occured.");

                return Ok(data);
            }
            catch (Exception ex) {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody]Garden data) {
            try {
                this._repo.UpdateGarden(data);
                int result = await this._repo.SaveChangesAsync();

                if (result == 0)
                    return BadRequest("An unexpected error occured.");

                return Ok(data);
            } catch (Exception ex) {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("get-types")]
        public async Task<IActionResult> GetPlantTypes()
            => Ok(await this._repo.GetPlantTypesAsync());

        [HttpPost("upload-photo")]
        public async Task<IActionResult> UploadPhoto() {
            try {
                var file = Request.Form.Files[0];
                var folderName = Path.Combine("Resources", "Images");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                if (file.Length > 0) {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var path = Path.Combine(folderName, fileName);

                    using (var stream = new FileStream(fullPath, FileMode.Create)) 
                        await file.CopyToAsync(stream);
                    
                    return Ok(new { fileName });
                } else 
                    return BadRequest();
                
            } catch (Exception ex) {
                return BadRequest($"Internal server error: {ex}");
            }
        }

        [HttpGet("seed")]
        public async Task<IActionResult> SeedTypesIfNotExists() {
            try {
                ICollection<PlantType> types = await this._repo.GetPlantTypesAsync();
                
                if (!types.Any()) {
                    ICollection<PlantType> plantTypesList = new List<PlantType> {
                        new PlantType {
                            Type = "Trees",
                            Plants = new List<Plant> {
                                new Plant { Name = "Clone" },
                                new Plant { Name = "Apple-tree" },
                                new Plant { Name = "Plum-tree" }
                            }
                        },
                        new PlantType {
                            Type = "Bushes",
                            Plants = new List<Plant> {
                                new Plant { Name = "Gooseberry" },
                                new Plant { Name = "Currant" },
                                new Plant { Name = "Raspberry" }
                            }
                        },
                        new PlantType {
                            Type = "Additional",
                            Plants = new List<Plant> {
                                new Plant { Name = "Grass" },
                                new Plant { Name = "Flowers" },
                                new Plant { Name = "Restricted-area" }
                            }
                        }
                    };

                    foreach(PlantType type in plantTypesList) 
                        this._repo.CreatePlantType(type);
                    
                    int result = await this._repo.SaveChangesAsync();

                    if (result == 0) return BadRequest("An unexpected error occured.");

                    return Ok();
                }
                else return BadRequest("The plant types already seeded.");
            } catch (Exception ex) {
                return BadRequest(ex.Message);
            }
        }
    }
}