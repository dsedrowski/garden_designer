using Microsoft.EntityFrameworkCore;

namespace api.Models
{
    public class DatabaseContext : DbContext
    {
        public virtual DbSet<Garden> Gardens { get; set; }
        public virtual DbSet<BackgroundImage> BackgroundImages { get; set; }
        public virtual DbSet<Plant> Plants { get; set; }
        public virtual DbSet<PlantType> PlantTypes { get; set; }
        public virtual DbSet<Square_Plant> Square_Plants { get; set; }
        public virtual DbSet<Square> Squares { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            optionsBuilder.UseSqlite("Filename=Database.db");
        }
    }
}