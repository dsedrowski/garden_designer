using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace api.Models
{
    public class Square_Plant
    {
        [Key]
        public int Id { get; set; }

        public int SquareId { get; set; }

        public int PlantId { get; set; }

        [ForeignKey("SquareId")]
        public virtual Square Square { get; set; }

        [ForeignKey("PlantId")]
        public virtual Plant Plant { get; set; }
    }
}