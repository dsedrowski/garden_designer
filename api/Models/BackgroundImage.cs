using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace api.Models
{
    public class BackgroundImage
    {
        [Key]
        public int Id { get; set; }
        public string Image { get; set; }

        public virtual ICollection<Garden> Gardens { get; set;}

        public BackgroundImage()
        {
            this.Gardens = new HashSet<Garden>();
        }
    }
}