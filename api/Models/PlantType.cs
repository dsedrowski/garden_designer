using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace api.Models
{
    public class PlantType
    {
        [Key]
        public int Id { get; set; }
        public string Type { get; set; }

        public virtual ICollection<Plant> Plants { get; set; }

        public PlantType()
        {
            this.Plants = new HashSet<Plant>();
        }
    }
}