using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace api.Models
{
    public class Square
    {
        [Key]
        public int Id { get; set; }
        public int GardenId { get; set; }
        public int Order { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        [ForeignKey("GardenId")]
        public virtual Garden Garden { get; set; }
        public virtual ICollection<Square_Plant> Square_Plants { get; set; }

        public Square()
        {
            this.Square_Plants = new HashSet<Square_Plant>();
        }
    }
}