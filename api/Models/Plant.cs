using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace api.Models
{
    public class Plant
    {
        [Key]
        public int Id { get; set; }
        
        public int PlantTypeId { get; set; }

        public string Name { get; set; }

        public string Image { get; set; }

        [ForeignKey("PlantTypeId")]
        public virtual PlantType PlantType { get; set; }

        public virtual ICollection<Square_Plant> Square_Plants { get; set; }

        public Plant()
        {
            this.Square_Plants = new HashSet<Square_Plant>();
        }
    }
}