using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace api.Models
{
    public class Garden
    {
        [Key]
        public int Id { get; set; }

        public int? BackgroundImageId { get; set; }

        public string Name { get; set; }

        [ForeignKey("BackgroundImageId")]
        public virtual BackgroundImage BackgroundImage { get; set; }

        public virtual ICollection<Square> Squares { get; set; }

        public Garden()
        {
            this.Squares = new HashSet<Square>();
        }
    }
}