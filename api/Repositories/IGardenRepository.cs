using System.Collections.Generic;
using System.Threading.Tasks;
using api.Models;

namespace api.Repositories
{
    public interface IGardenRepository
    {

         Task<ICollection<Garden>> GetGardensListAsync();
         Task<Garden> GetGardenAsync(int id);
         void CreateGarden(Garden data);
         void UpdateGarden(Garden data);
         Task<ICollection<PlantType>> GetPlantTypesAsync();
         void CreatePlantType(PlantType data);
         Task<int> SaveChangesAsync();
    }
}