using System.Collections.Generic;
using System.Threading.Tasks;
using api.Models;
using Microsoft.EntityFrameworkCore;

namespace api.Repositories
{
    public class GardenRepository : IGardenRepository
    {
        private readonly DatabaseContext _context;

        public GardenRepository(DatabaseContext context)
        {
            this._context = context;
        }

        public async Task<ICollection<Garden>> GetGardensListAsync()
            => await this._context.Gardens.Include(x => x.BackgroundImage).Include(x => x.Squares).ThenInclude(x => x.Square_Plants).ThenInclude(q => q.Plant).ThenInclude(q => q.Square_Plants).ToListAsync();

        public void CreateGarden(Garden data)
            => this._context.Gardens.Add(data);

        public async Task<Garden> GetGardenAsync(int id)
            => await this._context.Gardens.FindAsync(id);

        public async Task<int> SaveChangesAsync()
            => await this._context.SaveChangesAsync();

        public void UpdateGarden(Garden data) {
            if (data.BackgroundImage != null)
                this._context.Entry(data.BackgroundImage).State = EntityState.Modified;

            if (data.Squares != null) 
                foreach (Square square in data.Squares) {
                    if(square.Square_Plants != null)
                        foreach (Square_Plant square_Plant in square.Square_Plants)
                            this._context.Entry(square_Plant).State = (square_Plant.Id > 0) ? EntityState.Modified : EntityState.Added;

                    this._context.Entry(square).State = EntityState.Modified;
                }

            this._context.Entry(data).State = EntityState.Modified;
        }
            

        public async Task<ICollection<PlantType>> GetPlantTypesAsync()
            => await this._context.PlantTypes.Include(x => x.Plants).ToListAsync();
        

        public void CreatePlantType(PlantType data)
            => this._context.PlantTypes.Add(data);
    }
}